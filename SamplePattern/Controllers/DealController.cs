﻿namespace SamplePattern.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using SamplePattern.Helper;
    using SamplePattern.Model;
    using SamplePattern.Service;

    [Route("api/[controller]")]
    [ApiController]
    //https://adamstorr.azurewebsites.net/blog/aspnetcore-and-the-strategy-pattern
    public class DealController : ControllerBase
    {
        private readonly IDealStrategy _dealStrategy;
        public DealController(IDealStrategy dealStrategy)
        {
            _dealStrategy = dealStrategy;
        }

        [HttpGet("{id}")]
        public Deal Get(string id)
        {
            var setFeeDeal = _dealStrategy.GetDeal(id, DealEnum.SetFee);
            var revShareDeal = _dealStrategy.GetDeal(id, DealEnum.RevShare);
            return setFeeDeal;
        }
    }
}