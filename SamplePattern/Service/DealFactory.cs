﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace SamplePattern.Service
//{
//    public interface IDeal
//    {
//        void GetDeal();
//    }

//    public class SetFee : IDeal
//    {
//        public void GetDeal()
//        {
//            Console.WriteLine("Set fee deal");
//        }
//    }

//    public class RevShare : IDeal
//    {
//        public void GetDeal()
//        {
//            Console.WriteLine("Rev share deal");
//        }
//    }


//    public interface IDealFactory
//    {
//        IDeal GetDeal(string id);
//    }

//    public class SetFeeDealFactory : IDealFactory
//    {
//        public IDeal GetDeal(string id)
//        {           
//            return new SetFee();
//        }
//    }

//    public class RevShareDealFactory : IDealFactory
//    {
//        public IDeal GetDeal(string id)
//        {
//            return new RevShare();
//        }
//    }

   
//}
