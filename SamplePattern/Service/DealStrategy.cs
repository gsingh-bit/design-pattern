﻿namespace SamplePattern.Service
{
    using SamplePattern.Helper;
    using SamplePattern.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public interface IDealStrategy
    {
        Deal GetDeal(string id, DealEnum dealType);
    }

    public class DealStrategy : IDealStrategy
    {
        private readonly IEnumerable<IDealService> _dealServices;
        public DealStrategy(IEnumerable<IDealService> dealServices)
        {
            _dealServices = dealServices;
        }

        public Deal GetDeal(string id, DealEnum dealType)
        {
            return _dealServices.FirstOrDefault(x => x.DealType == dealType)?.GetDeal(id) ?? throw new ArgumentNullException(nameof(dealType));
        }
    }
}
