﻿namespace SamplePattern.Service
{
    using SamplePattern.Helper;
    using SamplePattern.Model;

    public interface IDealService
    {
        DealEnum DealType { get; }
        Deal GetDeal(string id);
    }

    public class SetFee : IDealService
    {
        public DealEnum DealType => DealEnum.SetFee;

        public Deal GetDeal(string id)
        {
            return new Deal() { DealType = "SetFee" };
        }
    }

    public class RevShare : IDealService
    {
        public DealEnum DealType => DealEnum.RevShare;

        public Deal GetDeal(string id)
        {
            return new Deal() { DealType = "RevShare" };
        }
    }
}
