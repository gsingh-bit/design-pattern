﻿namespace SamplePattern.Model
{
    public class Deal
    {
        public string Id { get; set; }
        public string DealType { get; set; }
    }

    public class SetFeeDeal : Deal
    {
        public string Name { get; set; }
    }

    public class RevShareDeal : Deal
    {
        public string Region { get; set; }
    }
}
